//
//  CommandItemsViewController.h
//  CarList
//
//  Created by Winparf on 2/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

#import "ViewController.h"
#import "Command.h"
@protocol CommandItemsViewControllerDelegate <NSObject>

-(void)didDoneCommand:(Command *)command;

@end

@interface CommandItemsViewController : UITableViewController
@property(nonatomic)BOOL isAdding;
@property(nonatomic,strong)Command *command;
@property(nonatomic,weak) id <CommandItemsViewControllerDelegate> delegate;
@property (nonatomic) NSInteger timeToIntersticial;
@end
