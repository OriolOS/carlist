//
//  Command.h
//  CarList
//
//  Created by Winparf on 3/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Command : NSObject
@property(nonatomic,strong)NSString *objectId;
@property(nonatomic,strong)NSString *title;
@property(nonatomic,strong)NSString *owner;
@property(nonatomic,strong)NSMutableArray *commandProducts;
@property(nonatomic,strong)NSMutableArray *sharedUsers;

@end
