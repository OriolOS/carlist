//
//  EmptyView.h
//  WinParfMobile
//
//  Created by Albert Villanueva on 16/3/15.
//  Copyright (c) 2015 WinParfServices. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface EmptyView : UIView

- (instancetype)initWithFrame:(CGRect)frame mainText:(NSString *)labelText andImage:(UIImage *)image;

@property (nonatomic, strong) UILabel *label;

@end
