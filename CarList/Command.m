//
//  Command.m
//  CarList
//
//  Created by Winparf on 3/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

#import "Command.h"

@implementation Command

-(NSMutableArray *)commandProducts{
    
    if (!_commandProducts) {
        _commandProducts = [[NSMutableArray alloc]init];
    }
    
    return _commandProducts;
}

-(NSMutableArray *)sharedUsers{
    
    if (!_sharedUsers) {
        _sharedUsers = [[NSMutableArray alloc]init];
    }
    
    return _sharedUsers;
}

@end
