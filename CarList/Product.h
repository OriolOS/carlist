//
//  CommandList.h
//  CarList
//
//  Created by Winparf on 3/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

#import <Foundation/Foundation.h>

@interface Product : NSObject

@property(nonatomic,strong) NSString *type;
@property(nonatomic,strong) NSString *product;
@property(nonatomic,strong) NSString *boughtFlag;
@end
