//
//  ViewController.m
//  CarList
//
//  Created by Winparf on 2/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

@import GoogleMobileAds;

#import <FBSDKCoreKit/FBSDKCoreKit.h>
#import <FBSDKLoginKit/FBSDKLoginKit.h>
#import <FBSDKShareKit/FBSDKShareKit.h>
#import <ParseFacebookUtilsV4/PFFacebookUtils.h>
#import "ViewController.h"
#import "CommandItemsViewController.h"
#import "EmptyView.h"
#import "Product.h"
#import "Parse/Parse.h"
#import <ParseUI/ParseUI.h>
#import "SWTableViewCell.h"

@interface ViewController ()<UITableViewDataSource, UITableViewDelegate,CommandItemsViewControllerDelegate,SWTableViewCellDelegate,PFLogInViewControllerDelegate, PFSignUpViewControllerDelegate>

@property(nonatomic,strong) NSMutableArray *commandList;
@property(nonatomic,strong) NSMutableArray *commandAllProducts;
@property (weak, nonatomic) IBOutlet UITableView *tableView;
@property(nonatomic,strong) NSIndexPath *indexPath;
@property(nonatomic,strong) EmptyView *emptyView;
@property(nonatomic,strong) PFLogInViewController *logInViewController;
@property(nonatomic,strong) PFLogInView *logInView;
@property(nonatomic,strong) PFSignUpViewController *signUpViewController;
@property (weak, nonatomic) IBOutlet GADBannerView *adBannerView;

@end

@implementation ViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the view, typically from a nib.
    
    self.timeToIntersticial = [[NSNumber alloc]initWithInt:0];
    
    NSMutableArray *leftBarButtons = [[NSMutableArray alloc]init];
    
    
    UIButton *logInButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [logInButton setImage:[UIImage imageNamed:@"LogIn"] forState:UIControlStateNormal];
    logInButton.frame = CGRectMake(0, 0, 25, 25);
    logInButton.showsTouchWhenHighlighted=YES;
    [logInButton addTarget:self action:@selector(logInButtonPressed:) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem1 = [[UIBarButtonItem alloc] initWithCustomView:logInButton];
    barButtonItem1.tintColor = [UIColor colorWithRed:0.0/255.0 green:122.0/255.0 blue:255.0/255.0 alpha:1.0];
    [leftBarButtons addObject:barButtonItem1];
    
    UIButton *facebookShareButton = [UIButton buttonWithType:UIButtonTypeCustom];
    [facebookShareButton setImage:[UIImage imageNamed:@"FacebookLogo.png"] forState:UIControlStateNormal];
    facebookShareButton.frame = CGRectMake(0, 0, 25, 25);
    facebookShareButton.showsTouchWhenHighlighted=YES;
    [facebookShareButton addTarget:self action:@selector(shareWithFacebook) forControlEvents:UIControlEventTouchUpInside];
    UIBarButtonItem *barButtonItem2 = [[UIBarButtonItem alloc] initWithCustomView:facebookShareButton];
    [leftBarButtons addObject:barButtonItem2];
    
    self.navigationItem.leftBarButtonItems = leftBarButtons;

    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(46.0/255.0) green:(204.0/255.0) blue:(113.0/255.0) alpha:1];
    
    if (!_commandList) {
        _commandList = [[NSMutableArray alloc]init];
    }
    
    if (!_commandAllProducts) {
        _commandAllProducts = [[NSMutableArray alloc]init];
    }
    
    if (!_logInViewController) {
        
        _logInViewController = [self resetLogInViewController];
    }
    if ([[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]) {
        
        NSString *objectId = [[[NSUserDefaults standardUserDefaults]objectForKey:@"UserFacebook"]objectForKey:@"objectId"];
        [self _loadDataWithId:objectId];
    }
    
    
    NSString *username = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    if (username.length > 0) {
        self.navigationItem.title = [NSString stringWithFormat:@"%@'s commands list", username];
    }
    
    NSLog(@"Google Mobile Ads SDK version: %@", [GADRequest sdkVersion]);
    // Replace this ad unit ID with your own ad unit ID.
    self.adBannerView.adUnitID = @"ca-app-pub-8715619194809926/8973087699";
    self.adBannerView.rootViewController = self;
    GADRequest *request = [GADRequest request];
    // Requests test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADBannerView automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [self.adBannerView loadRequest:request];
    
    

}

-(void)viewWillAppear:(BOOL)animated{

    if (![[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"] && ![[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]) {

        self.logInViewController = [self resetLogInViewController];
        
        [self presentViewController:self.logInViewController animated:YES completion:NULL];

    }else{
        [self getAllCommandsForUserLogged];
        
    }
    
}


-(void)getAllCommandsForUserLogged{
    
    [self.commandList removeAllObjects];
    NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    NSArray *userNames;
    if (userName.length > 0) {
        userNames = [[NSArray alloc]initWithObjects:userName, nil];
    }else{
        userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"name"];
        userNames = [[NSArray alloc]initWithObjects:userName, nil];
    }
    
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sharedUsers IN %@ OR owner = %@", userNames,userName];
    PFQuery *query = [PFQuery queryWithClassName:@"Commands" predicate:predicate];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            for (PFObject *object in objects) {
                Command *command = [[Command alloc]init];
                command.objectId = [object objectId];
                command.title = [object objectForKey:@"commandName"];
                command.owner = [object objectForKey:@"owner"];
                command.sharedUsers =[object objectForKey:@"sharedUsers"];
                NSMutableArray *arrayTypes = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productType"]];
                NSMutableArray *arrayDetails = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productDetail"]];
                NSMutableArray *boughtFlag = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"boughtFlag"]];
                
                [self.commandAllProducts removeAllObjects];
                int i = 0;
                for (NSString *detail in arrayDetails) {
                    Product *product = [[Product alloc]init];
                    product.product = detail;
                    product.type = [arrayTypes objectAtIndex:i];
                    product.boughtFlag = [boughtFlag objectAtIndex:i];
                    i++;
                    [self.commandAllProducts addObject:product];
                }
                
                
                command.commandProducts = [[NSMutableArray alloc]initWithArray:self.commandAllProducts];
                [self.commandList addObject:command];
            }
            if (!self.commandList.count) {
                self.emptyView = [[EmptyView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) mainText:@"No Commands" andImage:[UIImage imageNamed:@"CommandList"]];
                self.emptyView.alpha = 0.5;
                [self.view addSubview:self.emptyView];
            }else{
                
                [self.emptyView removeFromSuperview];
                [self.tableView reloadData];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];

    
}


-(void)addUserNameToShare:(UITextField *)textfield{
   
    PFQuery *query = [PFUser query];
    [query whereKey:@"username" equalTo:textfield.text];
    PFUser *user = (PFUser *)[query getFirstObject];

    if(user){
        Command *command = [self.commandList objectAtIndex:self.indexPath.row];
        [self.commandList removeObjectAtIndex:self.indexPath.row];
        [command.sharedUsers addObject:textfield.text];
        [self.commandList addObject:command];
        
        
    } else {
        if ([textfield.text length]) {
            // Log details of the failure
            NSString *alertLabel = [NSString stringWithFormat:@"There is no user with this username: %@", textfield.text];
            UIAlertController *alertController = [UIAlertController
                                                  alertControllerWithTitle:alertLabel
                                                  message:@"Please enter a valid "
                                                  preferredStyle:UIAlertControllerStyleAlert];
            UIAlertAction *okAction = [UIAlertAction
                                       actionWithTitle:@"OK"
                                       style:UIAlertActionStyleDefault
                                       handler:^(UIAlertAction *action)
                                       {
                                           [alertController dismissViewControllerAnimated:YES completion:nil];
                                       }];
            [alertController addAction:okAction];
            [self presentViewController:alertController animated:YES completion:nil];
        }
        
    }
    
}

- (IBAction)logInButtonPressed:(id)sender {
    
    self.logInViewController = [self resetLogInViewController];
    
    [self presentViewController:self.logInViewController animated:YES completion:nil];
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

-(void)prepareForSegue:(UIStoryboardSegue *)segue sender:(id)sender{
    
    if ([segue.identifier isEqualToString:@"addingCommand"]) {
        UINavigationController *nc = (UINavigationController *)segue.destinationViewController;
        CommandItemsViewController *vc = (CommandItemsViewController *)nc.topViewController;
        vc.isAdding = YES;
        vc.command = [[Command alloc]init];
        NSString *userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
        if (userName.length == 0) {
            userName = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserFacebook"]valueForKey:@"name"];
        }
        vc.command.owner = userName;
        vc.delegate = self;
    }
}

-(PFLogInViewController *)resetLogInViewController{
    
    self.logInViewController = nil;
    
    PFLogInViewController *logInVC = [[PFLogInViewController alloc]init];
    logInVC.fields = (PFLogInFieldsUsernameAndPassword
                                       | PFLogInFieldsLogInButton
                                       | PFLogInFieldsPasswordForgotten
                                       | PFLogInFieldsFacebook
                                       | PFLogInFieldsSignUpButton
                                       | PFLogInFieldsDismissButton);
    //Set event for facebook button
    [logInVC.logInView.facebookButton addTarget:self action:@selector(_loginWithFacebook) forControlEvents:UIControlEventTouchUpInside];
    [logInVC.logInView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoCarList.png"]]];
    [logInVC.logInView.logo setContentMode:UIViewContentModeScaleAspectFit];
    [logInVC setDelegate:self];
    self.signUpViewController = [[PFSignUpViewController alloc]init];
    [self.signUpViewController setDelegate:self];
    [logInVC setSignUpController:self.signUpViewController];
    [logInVC.signUpController.signUpView setLogo:[[UIImageView alloc] initWithImage:[UIImage imageNamed:@"logoCarList.png"]]];
    [logInVC.signUpController.signUpView.logo setContentMode:UIViewContentModeScaleAspectFit];
    logInVC.signUpController.delegate = self;
    
    return logInVC;
}

#pragma mark CommandItemsViewController Delegate

-(void)didDoneCommand:(Command *)command{
    
 //   [self getAllCommandsForUserLogged];
    
}


#pragma mark UITableView Datasource

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    
    return self.commandList.count;
}


- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    SWTableViewCell *cell = [tableView dequeueReusableCellWithIdentifier:@"commandCell"];

    Command *command = [self.commandList objectAtIndex:indexPath.row];
    NSString *cellTitle = command.title;
    NSString *sharedWith = [[NSString alloc]init];
    sharedWith = [NSString stringWithFormat:@"Owner: %@ ", command.owner] ;
        if (command.sharedUsers.count != 0) {
            
            sharedWith = [NSString stringWithFormat:@"%@  and shared with: ",sharedWith];
            int i;
            for (i = 0 ; i < command.sharedUsers.count - 1; i++) {
                
                sharedWith = [NSString stringWithFormat:@"%@%@, ",sharedWith ,[command.sharedUsers objectAtIndex:i]];
            }
            sharedWith = [NSString stringWithFormat:@"%@%@ ",sharedWith ,[command.sharedUsers objectAtIndex:i]];
        }
    
    cell.detailTextLabel.adjustsFontSizeToFitWidth = YES;
    cell.detailTextLabel.text = sharedWith;
    cell.textLabel.text =cellTitle;
    cell.rightUtilityButtons = [self rightButtons];
    cell.leftUtilityButtons = [self leftButtons];
    cell.delegate = self;
    
    return cell;
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{

    UINavigationController *nc = [self.storyboard instantiateViewControllerWithIdentifier:@"commandNC"];
    CommandItemsViewController *vc = (CommandItemsViewController *)nc.topViewController;
    vc.command = [self.commandList objectAtIndex:indexPath.row];
    [self.commandList removeObjectAtIndex:indexPath.row];
    vc.delegate = self;
    [self presentViewController:nc animated:YES completion:nil];
}


#pragma mark SWTableviewCell

- (void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerRightUtilityButtonWithIndex:(NSInteger)index{
        
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        
        Command *command = [self.commandList objectAtIndex:indexPath.row];
        NSString *objectId = command.objectId;
        
        PFObject *object = [PFObject objectWithoutDataWithClassName:@"Commands" objectId:objectId];
        [object deleteEventually];
        
        [self.commandList removeObjectAtIndex:indexPath.row];
        
        [self.tableView reloadData];    
    
}

-(void)swipeableTableViewCell:(SWTableViewCell *)cell didTriggerLeftUtilityButtonWithIndex:(NSInteger)index{
    
        NSIndexPath *indexPath = [self.tableView indexPathForCell:cell];
        self.indexPath = indexPath;
        Command *command = [self.commandList objectAtIndex:indexPath.row];
        NSString *objectId = command.objectId;
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"Enter a user name to share the command"
                                              message:@""
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
         {
             textField.placeholder = @"User name";
             [textField addTarget:self action:@selector(addUserNameToShare:) forControlEvents:UIControlEventEditingDidEnd];
             
         }];
        
        
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"ADD"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       
                                       Command *command = [self.commandList objectAtIndex:indexPath.row];
                                       
                                       PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
                                       
                                       // Update Command products
                                       [query getObjectInBackgroundWithId:command.objectId
                                                                    block:^(PFObject *object, NSError *error) {
                                                                        
                                                                        object[@"sharedUsers"] = command.sharedUsers;
                                                                        
                                                                        [object saveInBackground];
                                                                    }];
                                       
                                       [self.tableView reloadData];
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        UIAlertAction *cancel = [UIAlertAction
                                 actionWithTitle:@"Cancel"
                                 style:UIAlertActionStyleCancel
                                 handler:^(UIAlertAction *action)
                                 {
                                     [alertController dismissViewControllerAnimated:YES completion:nil];
                                 }];
        [alertController addAction:cancel];
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    
}

- (BOOL)swipeableTableViewCellShouldHideUtilityButtonsOnSwipe:(SWTableViewCell *)cell
{
    // allow just one cell's utility button to be open at once
    return YES;
}

- (NSArray *)rightButtons
{
    NSMutableArray *rightUtilityButtons = [NSMutableArray new];
    [rightUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:0.231f blue:0.188 alpha:1.0f] icon:[UIImage imageNamed:@"Delete.png"]];
    return rightUtilityButtons;
}

- (NSArray *)leftButtons
{
    NSMutableArray *leftUtilityButtons = [NSMutableArray new];
    [leftUtilityButtons sw_addUtilityButtonWithColor:[UIColor colorWithRed:1.0f green:1.0f blue:0.35f alpha:0.7] icon:[UIImage imageNamed:@"Share.png"]];
    return leftUtilityButtons;
}

#pragma mark PFLogInViewController Delegate

- (void)logInViewController:(PFLogInViewController *)logInController didLogInUser:(PFUser *)user{
    
    
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userDict = @{
                           @"username" : user.username,
                           @"email" : user.email};
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults removeObjectForKey:@"UserFacebook"];
    [defaults synchronize];
    
    NSString *username = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    if (username.length > 0) {
        self.navigationItem.title = [NSString stringWithFormat:@"%@'s command list", username];
    }
    
    [self.commandList removeAllObjects];
    NSArray *userNames = [[NSArray alloc]initWithObjects:user.username, nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sharedUsers IN %@", userNames];
    PFQuery *query = [PFQuery queryWithClassName:@"Commands" predicate:predicate];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            for (PFObject *object in objects) {
                Command *command = [[Command alloc]init];
                command.objectId = [object objectId];
                command.title = [object objectForKey:@"commandName"];
                command.owner =[object objectForKey:@"owner"];
                command.sharedUsers =[object objectForKey:@"sharedUsers"];
                NSMutableArray *arrayTypes = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productType"]];
                NSMutableArray *arrayDetails = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productDetail"]];
                NSMutableArray *boughtFlag = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"boughtFlag"]];
                
                [self.commandAllProducts removeAllObjects];
                int i = 0;
                for (NSString *detail in arrayDetails) {
                    Product *product = [[Product alloc]init];
                    product.product = detail;
                    product.type = [arrayTypes objectAtIndex:i];
                    product.boughtFlag = [boughtFlag objectAtIndex:i];
                    i++;
                    [self.commandAllProducts addObject:product];
                }
                
                
                command.commandProducts = [[NSMutableArray alloc]initWithArray:self.commandAllProducts];
                [self.commandList addObject:command];
            }
            if (!self.commandList.count) {
                self.emptyView = [[EmptyView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) mainText:@"No Commands" andImage:[UIImage imageNamed:@"CommandList"]];
                self.emptyView.alpha = 0.5;
                [self.view addSubview:self.emptyView];
            }else{
                [self.emptyView removeFromSuperview];
                [self.tableView reloadData];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];

}


-(void)signUpViewController:(PFSignUpViewController * __nonnull)signUpController didSignUpUser:(PFUser * __nonnull)user{
    
    NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
    NSDictionary *userDict = @{
                               @"username" : user.username,
                               @"email" : user.email};
    [defaults setObject:userDict forKey:@"UserApp"];
    [defaults removeObjectForKey:@"UserFacebook"];
    [defaults synchronize];
    
    NSString *username = [[[NSUserDefaults standardUserDefaults] objectForKey:@"UserApp"]valueForKey:@"username"];
    if (username.length > 0) {
        self.navigationItem.title = [NSString stringWithFormat:@"%@'s command list", username];
    }
    
    [self.commandList removeAllObjects];
    NSArray *userNames = [[NSArray alloc]initWithObjects:user.username, nil];
    NSPredicate *predicate = [NSPredicate predicateWithFormat:@"sharedUsers IN %@", userNames];
    PFQuery *query = [PFQuery queryWithClassName:@"Commands" predicate:predicate];
    [query findObjectsInBackgroundWithBlock:^(NSArray *objects, NSError *error) {
        if (!error) {
            
            for (PFObject *object in objects) {
                Command *command = [[Command alloc]init];
                command.objectId = [object objectId];
                command.title = [object objectForKey:@"commandName"];
                command.owner =[object objectForKey:@"owner"];
                command.sharedUsers =[object objectForKey:@"sharedUsers"];
                NSMutableArray *arrayTypes = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productType"]];
                NSMutableArray *arrayDetails = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"productDetail"]];
                NSMutableArray *boughtFlag = [[NSMutableArray alloc]initWithArray:[object objectForKey:@"boughtFlag"]];
                
                [self.commandAllProducts removeAllObjects];
                int i = 0;
                for (NSString *detail in arrayDetails) {
                    Product *product = [[Product alloc]init];
                    product.product = detail;
                    product.type = [arrayTypes objectAtIndex:i];
                    product.boughtFlag = [boughtFlag objectAtIndex:i];
                    i++;
                    [self.commandAllProducts addObject:product];
                }
                
                
                command.commandProducts = [[NSMutableArray alloc]initWithArray:self.commandAllProducts];
                [self.commandList addObject:command];
            }
            if (!self.commandList.count) {
                self.emptyView = [[EmptyView alloc]initWithFrame:CGRectMake(0, 0, self.view.frame.size.width, self.view.frame.size.height) mainText:@"No Commands" andImage:[UIImage imageNamed:@"CommandList"]];
                self.emptyView.alpha = 0.5;
                [self.view addSubview:self.emptyView];
            }else{
                [self.emptyView removeFromSuperview];
                [self.tableView reloadData];
            }
        } else {
            // Log details of the failure
            NSLog(@"Error: %@ %@", error, [error userInfo]);
        }
    }];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

#pragma mark Facebook API

- (void)_loginWithFacebook {
    // Set permissions required from the facebook user account
    NSArray *permissionsArray = @[ @"user_about_me", @"user_relationships", @"user_birthday", @"user_location",@"email"];
    
    // Login PFUser using Facebook
    [PFFacebookUtils logInInBackgroundWithReadPermissions:permissionsArray block:^(PFUser *user, NSError *error) {
        if (!user) {
            NSLog(@"Uh oh. The user cancelled the Facebook login.");
        } else if (user.isNew) {
            NSString *objectId = user.objectId;
            [self _loadDataWithId:objectId];
            NSLog(@"User signed up and logged in through Facebook!");
        } else {
            NSString *objectId = user.objectId;
            [self _loadDataWithId:objectId];
            NSLog(@"User logged in through Facebook!");
        }
    }];
}


- (void)_loadDataWithId:(NSString *)objectId {
    
    FBSDKGraphRequest *request = [[FBSDKGraphRequest alloc] initWithGraphPath:@"me" parameters:nil];
    [request startWithCompletionHandler:^(FBSDKGraphRequestConnection *connection, id result, NSError *error) {
        if (!error) {
            // result is a dictionary with the user's Facebook data
            NSMutableDictionary *userData = [(NSDictionary *)result mutableCopy];
            [userData setObject:objectId forKey:@"objectId"];
            
            NSString *facebookID = userData[@"id"];
            NSString *name = userData[@"name"];
            NSString *location = userData[@"location"][@"name"];
            NSString *gender = userData[@"gender"];
            NSString *birthday = userData[@"birthday"];
            NSString *email = userData[@"email"];
            NSString *relationship = userData[@"relationship_status"];
            
            NSURL *pictureURL = [NSURL URLWithString:[NSString stringWithFormat:@"https://graph.facebook.com/%@/picture?type=large&return_ssl_resources=1", facebookID]];
            
            NSUserDefaults *defaults = [NSUserDefaults standardUserDefaults];
            [defaults setObject:userData forKey:@"UserFacebook"];
            [defaults removeObjectForKey:@"UserApp"];
            [defaults synchronize];
            
            if (name.length > 0) {
                self.navigationItem.title = [NSString stringWithFormat:@"%@'s command list", name];
            }
            [self changeUserNameToFacebookName:name withEmail:email withId:objectId];
        
            [self shareWithFacebook];
        }
        
    }];
    
    
}

-(void)changeUserNameToFacebookName:(NSString *)facebookName withEmail:(NSString *)email withId:(NSString *)objectId{
    
    PFQuery *query = [PFUser query];
    [query whereKey:@"objectId" equalTo:objectId];
    [query getFirstObjectInBackgroundWithBlock:^(PFObject *user, NSError *error) {
                                     // Now let's update it with some new data. In this case, only cheatMode and score
                                     // will get sent to the cloud. playerName hasn't changed.

                                     user[@"username"] = facebookName;
                                     user[@"email"] = email;
                                     
                                     [user saveInBackground];
        
                                     [self dismissViewControllerAnimated:YES completion:nil];
//                                     [self getAllCommandsForUserLogged];
                                 }];

}

-(void)shareWithFacebook{
    
    if([PFFacebookUtils isLinkedWithUser:[PFUser currentUser]]){
        
        
        
        NSMutableArray *array = [NSMutableArray arrayWithObject:@"publish_actions"];
        [PFFacebookUtils logInInBackgroundWithPublishPermissions:array block:^(PFUser *user, NSError *error){
            
        }];
        
        FBSDKShareLinkContent *content = [[FBSDKShareLinkContent alloc] init];
        content.contentURL = [NSURL URLWithString:@"https://www.facebook.com/games/?fbs=-1&app_id=844893465581256&preview=1&locale=ca_ES"];
        [FBSDKShareDialog showFromViewController:self
                                     withContent:content
                                        delegate:nil];
        
        
    }else{
        
        UIAlertController *alertController = [UIAlertController
                                              alertControllerWithTitle:@"This user is not logged in Facebook"
                                              message:@"Please log with Facebook"
                                              preferredStyle:UIAlertControllerStyleAlert];
        
        UIAlertAction *okAction = [UIAlertAction
                                   actionWithTitle:@"OK"
                                   style:UIAlertActionStyleDefault
                                   handler:^(UIAlertAction *action)
                                   {
                                       [alertController dismissViewControllerAnimated:YES completion:nil];
                                   }];
        
        [alertController addAction:okAction];
        [self presentViewController:alertController animated:YES completion:nil];
    }
}


- (void)_logOut  {
    [PFUser logOut]; // Log out
}



@end