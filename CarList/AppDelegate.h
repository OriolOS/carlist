//
//  AppDelegate.h
//  CarList
//
//  Created by Winparf on 2/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

#import <UIKit/UIKit.h>

@interface AppDelegate : UIResponder <UIApplicationDelegate>

@property (strong, nonatomic) UIWindow *window;


@end

