//
//  CommandItemsViewController.m
//  CarList
//
//  Created by Winparf on 2/6/15.
//  Copyright (c) 2015 Winparf. All rights reserved.
//

@import GoogleMobileAds;
#import "CommandItemsViewController.h"
#import "Product.h"
#import "Parse/Parse.h"

@interface CommandItemsViewController ()<UITableViewDataSource, UITableViewDelegate,GADInterstitialDelegate,UITextFieldDelegate>
@property(nonatomic,strong)Product *product;
@property(nonatomic,strong)NSMutableDictionary *list;
@property(nonatomic,strong)NSMutableArray *arrayAllProducts;
@property(nonatomic,strong)NSMutableArray *productOfKey;
@property(nonatomic,strong)NSMutableArray *boughtFlag;
@property(nonatomic,strong)NSMutableArray *autoCompleteKeys;
@property(nonatomic,strong)NSString *detail;
@property(nonatomic,strong)NSString *type;
@property(nonatomic,strong)UITextField *typeTextField;
@property(nonatomic,strong)NSMutableArray *keys;
@property (strong, nonatomic) IBOutlet UITableView *tableView;
@property (nonatomic,strong) UITableView *autocCompletitionTableView;
@property (nonatomic,strong) GADInterstitial *adIntersticialView;
@property (nonatomic)BOOL isCancel;

@end

@implementation CommandItemsViewController

- (void)viewDidLoad {
    [super viewDidLoad];
    // Do any additional setup after loading the
    
    self.timeToIntersticial = 0;
    
    if(!self.list){
        self.list = [[NSMutableDictionary alloc]init];
    }
    if (!self.productOfKey) {
        self.productOfKey = [[NSMutableArray alloc]init];
    }
    
    if (!self.arrayAllProducts) {
        self.arrayAllProducts = [[NSMutableArray alloc]init];
    }
    if (!self.autoCompleteKeys) {
        self.autoCompleteKeys = [[NSMutableArray alloc]init];
    }
    
    if (!self.boughtFlag) {
        self.boughtFlag = [[NSMutableArray alloc]init];
    }
    
    if (!self.keys) {
        self.keys = [[NSMutableArray alloc]init];
    }
    
    if (!self.command.title) {
        [self getTitleForCommand];
    }else{
        self.navigationItem.title = self.command.title;
        self.arrayAllProducts = self.command.commandProducts;
        
    }
    
    for (Product *product in self.arrayAllProducts) {
        if (![self.keys containsObject:product.type]) {
            [self.keys addObject:product.type];
        }
    }
    
    if (self.arrayAllProducts.count > 0) {
        [self.tableView reloadData];
    }
    
    self.navigationController.navigationBar.backgroundColor = [UIColor colorWithRed:(52.0/255.0) green:(152.0/255.0) blue:(219.0/255.0) alpha:1];
    
}

-(void)viewWillAppear:(BOOL)animated{
    
    if (!self.command.objectId){
        
        PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
        
        // Update Command products
        [query getObjectInBackgroundWithId:self.command.objectId
                                     block:^(PFObject *commnand, NSError *error) {
                                         // Now let's update it with some new data. In this case, only cheatMode and score
                                         // will get sent to the cloud. playerName hasn't changed.
                                         NSMutableArray *arrayTypes = [[NSMutableArray alloc]init];
                                         NSMutableArray *arrayDetails = [[NSMutableArray alloc]init];
                                         NSMutableArray *boughtFlag = [[NSMutableArray alloc]init];
                                         for (Product *product in self.arrayAllProducts) {
                                             [arrayTypes addObject:product.type];
                                             [arrayDetails addObject:product.product];
                                             [boughtFlag addObject:product.boughtFlag];
                                         }
                                         commnand[@"productType"] = arrayTypes;
                                         commnand[@"productDetail"] = arrayDetails;
                                         commnand[@"boughtFlag"] = boughtFlag;
                                         commnand[@"sharedUsers"] = self.command.sharedUsers;
                                         
                                         [commnand saveInBackground];
                                     }];
    }
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}


- (IBAction)doneButtonPressed:(id)sender {
    

    
    [self.delegate didDoneCommand:nil];
    
    [self dismissViewControllerAnimated:YES completion:nil];
}

- (IBAction)addProductsButtonPressed:(id)sender {
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Enter a product"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         self.typeTextField = textField;
         textField.delegate = self;
         textField.placeholder = @"Type of product";
         [textField addTarget:self action:@selector(addTypeProduct:) forControlEvents:UIControlEventEditingDidEnd];
         
     }];
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Detail of product";
         [textField addTarget:self action:@selector(addDetailProduct:) forControlEvents:UIControlEventEditingDidEnd];
         
     }];
    
    
    self.autocCompletitionTableView = [[UITableView alloc] initWithFrame:CGRectMake(0, 93, 253, 120) style:UITableViewStylePlain];
    self.autocCompletitionTableView.delegate = self;
    self.autocCompletitionTableView.dataSource = self;
    self.autocCompletitionTableView.scrollEnabled = YES;
    self.autocCompletitionTableView.hidden = YES;
    [alertController.view addSubview:self.autocCompletitionTableView];

    
    UIAlertAction *okAction = [UIAlertAction
                     actionWithTitle:@"ADD"
                     style:UIAlertActionStyleDefault
                     handler:^(UIAlertAction *action)
                     {
                         self.timeToIntersticial++;
                         self.product = [[Product alloc]init];
                         [self.product setType:self.type];
                         [self.product setProduct:self.detail];
                         [self.product setBoughtFlag:@"0"];
                         [self.arrayAllProducts addObject:self.product];
                         
                         if (![self.keys containsObject:self.product.type]) {
                             [self.keys addObject:self.product.type];
                         }
                         
                         for (Product *product in self.arrayAllProducts) {
                             if ([product.type isEqualToString:self.type]) {
                                 [self.productOfKey addObject:product];
                             }
                         }
                         
                         [self.list setObject:self.productOfKey forKey:self.type];
                         
                         if (!self.productOfKey) {
                             self.productOfKey = [[NSMutableArray alloc]initWithObjects:self.product, nil];
                         }else{
                             [self.productOfKey addObject:self.product];
                         }
                         
                         PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
                         
                         // Update Command products
                         [query getObjectInBackgroundWithId:self.command.objectId
                                                      block:^(PFObject *commnand, NSError *error) {
                                                          // Now let's update it with some new data. In this case, only cheatMode and score
                                                          // will get sent to the cloud. playerName hasn't changed.
                                                          NSMutableArray *arrayTypes = [[NSMutableArray alloc]init];
                                                          NSMutableArray *arrayDetails = [[NSMutableArray alloc]init];
                                                          NSMutableArray *boughtFlag = [[NSMutableArray alloc]init];
                                                          for (Product *product in self.arrayAllProducts) {
                                                              [arrayTypes addObject:product.type];
                                                              [arrayDetails addObject:product.product];
                                                              [boughtFlag addObject:product.boughtFlag];
                                                          }
                                                          commnand[@"productType"] = arrayTypes;
                                                          commnand[@"productDetail"] = arrayDetails;
                                                          commnand[@"boughtFlag"] = boughtFlag;
                                                          commnand[@"owner"] = self.command.owner;
                                                          commnand[@"sharedUsers"] = self.command.sharedUsers;

                                                          [commnand saveInBackground];
                                                      }];
                         
                         [self.tableView reloadData];
                         [alertController dismissViewControllerAnimated:YES completion:nil];
                     }];

    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action)
                             {
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    [self presentViewController:alertController animated:YES completion:nil];
    

    
}

- (void)addDetailProduct:(UITextField *)textfield{
    
    self.detail = textfield.text;
}
- (void)addTypeProduct:(UITextField *)textfield{
    if ([textfield.text isEqualToString:@""]) {
        self.type = @"Basic Items";
    }else {
        
        self.type = textfield.text;
    }
}

-(void)getTitleForCommand{
    
    UIAlertController *alertController = [UIAlertController
                                          alertControllerWithTitle:@"Command Title"
                                          message:@""
                                          preferredStyle:UIAlertControllerStyleAlert];
    
    [alertController addTextFieldWithConfigurationHandler:^(UITextField *textField)
     {
         textField.placeholder = @"Title for this command";
         [textField addTarget:self action:@selector(addTitleToCommand:) forControlEvents:UIControlEventEditingDidEnd];
         
     }];
   
    
    
    UIAlertAction *okAction = [UIAlertAction
                               actionWithTitle:@"Ok"
                               style:UIAlertActionStyleDefault
                               handler:^(UIAlertAction *action)
                               {

                                   
                                   self.navigationItem.title = self.command.title;
                                   [alertController dismissViewControllerAnimated:YES completion:nil];
                               }];
    UIAlertAction *cancel = [UIAlertAction
                             actionWithTitle:@"Cancel"
                             style:UIAlertActionStyleCancel
                             handler:^(UIAlertAction *action)
                             {
                                 self.isCancel = YES;
                                 [alertController dismissViewControllerAnimated:YES completion:nil];
                                 [self dismissViewControllerAnimated:YES completion:nil];
                             }];
    [alertController addAction:cancel];
    [alertController addAction:okAction];
    [self.autocCompletitionTableView removeFromSuperview];
    [self presentViewController:alertController animated:YES completion:nil];
    
}

-(void)addTitleToCommand:(UITextField *)textfield{
    
    PFObject *command = [PFObject objectWithClassName:@"Commands"];
    command[@"commandName"] = textfield.text;
    command[@"owner"] = self.command.owner;
    command[@"sharedUsers"] = self.command.sharedUsers;
    [command saveInBackgroundWithBlock:^(BOOL succeeded, NSError *error) {
        
        if(succeeded){
            self.command.objectId = command.objectId;
        }
        if (self.isCancel) {
            [self deleteCommandAfterCancel];
            self.isCancel = NO;
        }
        
    }];
    
    
    self.command.title = textfield.text;
}

-(void)deleteCommandAfterCancel{
    
    PFObject *object = [PFObject objectWithoutDataWithClassName:@"Commands"
                                                       objectId:self.command.objectId];
    [object deleteEventually];
}



#pragma mark UITableView Datasource
-(NSInteger)numberOfSectionsInTableView:(UITableView *)tableView{
    
    NSInteger numberOfSections;
    
    if ([tableView isEqual:self.autocCompletitionTableView]) {
        numberOfSections = 1;
    }else{
        if (!self.keys) {
            numberOfSections = 1;
        }else{
            numberOfSections = self.keys.count;
        }
    }

    
    return numberOfSections;
}
- (NSString *)tableView:(UITableView *)tableView titleForHeaderInSection:(NSInteger)section{

    NSString *title;
    
    if ([tableView isEqual:self.autocCompletitionTableView]) {
        title = nil;
    }else{
        
        if (!self.keys) {
            title = @"Basic";
        }else{
            title = [self.keys objectAtIndex:section];
        }
    }
    return title;
}

- (NSInteger)tableView:(UITableView *)tableView numberOfRowsInSection:(NSInteger)section{
    
    NSInteger numberOfRows;
    
    if ([tableView isEqual:self.autocCompletitionTableView]) {
        if (self.autoCompleteKeys.count) {
            numberOfRows = self.autoCompleteKeys.count;
        }else{
            numberOfRows = 0;
        }
    }else{
        if (!self.command.commandProducts) {
            numberOfRows = 0;
        }else{
            NSMutableArray *products = [[NSMutableArray alloc]init];
            for (Product *product in self.arrayAllProducts) {
                if ([product.type isEqualToString:[self.keys objectAtIndex:section]]) {
                    [products addObject:product];
                }
            }
            if ([products isKindOfClass:[NSArray class]]) {
                numberOfRows = products.count;
            }else{
                numberOfRows = 1;
            }
        }
    }
    
    return numberOfRows;
    
}

- (UITableViewCell *)tableView:(UITableView *)tableView cellForRowAtIndexPath:(NSIndexPath *)indexPath{
    
    UITableViewCell *cell = [self.tableView  dequeueReusableCellWithIdentifier:@"productCell"];
    
    if ([tableView isEqual:self.autocCompletitionTableView]) {
        
        cell.textLabel.text = [self.autoCompleteKeys objectAtIndex:indexPath.row];
        
    }else{
        
        NSMutableArray *products = [[NSMutableArray alloc]init];
        for (Product *product in self.arrayAllProducts) {
            if ([product.type isEqualToString:[self.keys objectAtIndex:indexPath.section]]) {
                [products addObject:product];
            }
        }
        
        if ([products isKindOfClass:[NSArray class]]) {
            Product *product = [products objectAtIndex:indexPath.row];
            cell.textLabel.text = product.product;
            if ([product.boughtFlag isEqualToString:@"1"]) {
                
                
                cell.imageView.image = [UIImage imageNamed:@"Checked.png"];
                
            }else{
                cell.imageView.image = nil;
            }
        }else{
            cell.textLabel.text = products;
            
        }
    }
    
    //Load Add Interstitial
    if (self.timeToIntersticial >= 5) {
        
        [self setInterstitialAd];
        self.timeToIntersticial = 0;
    }
    
    return cell;
}

#pragma mark UITableView Delegate

- (void)tableView:(UITableView *)tableView didSelectRowAtIndexPath:(NSIndexPath *)indexPath{
    
    if ([tableView isEqual:self.autocCompletitionTableView]) {
        NSString *key = [self.autoCompleteKeys objectAtIndex:indexPath.row];
        self.typeTextField.text = key;
        [self.autocCompletitionTableView removeFromSuperview];
        
    }else{
        NSMutableArray *products = [[NSMutableArray alloc]init];
        for (Product *product in self.arrayAllProducts) {
            if ([product.type isEqualToString:[self.keys objectAtIndex:indexPath.section]]) {
                [products addObject:product];
            }
        }
        Product *product = [products objectAtIndex:indexPath.row];
        
        if ([product.boughtFlag isEqualToString:@"0"]) {
            product.boughtFlag = @"1";
            
            // Update Command products
            
            PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
            
            [query getObjectInBackgroundWithId:self.command.objectId
                                         block:^(PFObject *commnand, NSError *error) {
                                             // Now let's update it with some new data. In this case, only cheatMode and score
                                             // will get sent to the cloud. playerName hasn't changed.
                                             NSMutableArray *boughtFlag = [[NSMutableArray alloc]init];
                                             for (Product *product in self.arrayAllProducts) {
                                                 
                                                 [boughtFlag addObject:product.boughtFlag];
                                             }
                                             
                                             commnand[@"boughtFlag"] = boughtFlag;
                                             
                                             [commnand saveInBackground];
                                         }];
            
        }else{
            product.boughtFlag = @"0";
            
            // Update Command products
            
            PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
            
            [query getObjectInBackgroundWithId:self.command.objectId
                                         block:^(PFObject *commnand, NSError *error) {
                                             // Now let's update it with some new data. In this case, only cheatMode and score
                                             // will get sent to the cloud. playerName hasn't changed.
                                             NSMutableArray *boughtFlag = [[NSMutableArray alloc]init];
                                             for (Product *product in self.arrayAllProducts) {
                                                 
                                                 [boughtFlag addObject:product.boughtFlag];
                                             }
                                             
                                             commnand[@"boughtFlag"] = boughtFlag;
                                             
                                             [commnand saveInBackground];
                                         }];
            
        }
        [self.tableView reloadData];
    }
}

- (BOOL)tableView:(UITableView *)tableView canEditRowAtIndexPath:(NSIndexPath *)indexPath
{
    // Return YES - we will be able to delete all rows
    return YES;
}

-(void)tableView:(UITableView *)tableView commitEditingStyle:(UITableViewCellEditingStyle)editingStyle forRowAtIndexPath:(NSIndexPath *)indexPath {
    
    NSUInteger indexArray = 0;
    
    self.timeToIntersticial++;
    
    for (int sections = 0 ; sections < indexPath.section; sections++) {
        indexArray += [self.tableView numberOfRowsInSection:sections];
    }
    
    indexArray += indexPath.row;
    Product *product = [self.arrayAllProducts objectAtIndex:indexArray];
    
    [self.arrayAllProducts removeObject:product];
    
    [self.keys removeAllObjects];
    for (Product *product in self.arrayAllProducts) {
        if (![self.keys containsObject:product.type]) {
            [self.keys addObject:product.type];
        }
    }
    
    
    PFQuery *query = [PFQuery queryWithClassName:@"Commands"];
    
    // Update Command products
    [query getObjectInBackgroundWithId:self.command.objectId
                                 block:^(PFObject *commnand, NSError *error) {
                                     // Now let's update it with some new data. In this case, only cheatMode and score
                                     // will get sent to the cloud. playerName hasn't changed.
                                     NSMutableArray *arrayTypes = [[NSMutableArray alloc]init];
                                     NSMutableArray *arrayDetails = [[NSMutableArray alloc]init];
                                     NSMutableArray *boughtFlag = [[NSMutableArray alloc]init];
                                     for (Product *product in self.arrayAllProducts) {
                                         [arrayTypes addObject:product.type];
                                         [arrayDetails addObject:product.product];
                                         [boughtFlag addObject:product.boughtFlag];
                                     }
                                     commnand[@"productType"] = arrayTypes;
                                     commnand[@"productDetail"] = arrayDetails;
                                     commnand[@"boughtFlag"] = boughtFlag;
                                     
                                     [commnand saveInBackground];
                                 }];

    
    
    
    [self.tableView reloadData];
}
#pragma mark Set Interstitial AD

-(void) setInterstitialAd{
    
    self.adIntersticialView = [[GADInterstitial alloc] initWithAdUnitID:@"ca-app-pub-8715619194809926/2088949297"];
    self.adIntersticialView.delegate = self;
    
    GADRequest *request = [GADRequest request];
    // Request test ads on devices you specify. Your test device ID is printed to the console when
    // an ad request is made. GADInterstitial automatically returns test ads when running on a
    // simulator.
    request.testDevices = @[ kGADSimulatorID ];
    [self.adIntersticialView loadRequest:request];
}

#pragma mark GADIntersticialDelegate

-(void)interstitialDidReceiveAd:(GADInterstitial *)ad{
    

    [self.adIntersticialView presentFromRootViewController:self];

}


#pragma mark Key pressed methods AUTOCOMPLETION TYPE Textfield

- (BOOL)textField:(UITextField *)textField
shouldChangeCharactersInRange:(NSRange)range
replacementString:(NSString *)string {
    self.autocCompletitionTableView.hidden = NO;
    
    NSString *substring = [NSString stringWithString:textField.text];
    substring = [substring
                 stringByReplacingCharactersInRange:range withString:string];
    [self searchAutocompleteEntriesWithSubstring:substring];
    return YES;
}
- (void)searchAutocompleteEntriesWithSubstring:(NSString *)substring {
    
    // Put anything that starts with this substring into the autocompleteUrls array
    // The items in this array is what will show up in the table view
    [self.autoCompleteKeys removeAllObjects];
    for(NSString *key in self.keys) {
        NSRange substringRange = [key rangeOfString:substring];
        if (substringRange.location == 0) {
            [self.autoCompleteKeys addObject:key];
        }
    }
    
    if (self.autoCompleteKeys.count) {
        self.autocCompletitionTableView.hidden = NO;
        [self.autocCompletitionTableView reloadData];
    } else{
        self.autocCompletitionTableView.hidden = YES;
    }
}


    
@end
